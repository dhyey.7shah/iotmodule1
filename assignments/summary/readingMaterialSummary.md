# **Industrial Internet of Things**
<img src="https://servicegeeni.com/wp-content/uploads/2017/04/iiot.png" height="400" width="600" >

## Industrial Revolution
<img src="https://i.pinimg.com/originals/93/03/61/9303614240a2732b951fbb47cac1efd9.png" height="700" width="900">

1. **Industry 1.0**: The First Industrial Revolution began in the 18th century through the use of steam power and mechanisation of production.
2. **Industry 2.0**: The Second Industrial Revolution began in the 19th century through the discovery of electricity and assembly line production. 
3. **Industry 3.0**: The Third Industrial Revolution began in the ’70s in the 20th century through partial automation using memory-programmable controls and computers.
4. **Industry 4.0**: This is characterised by the application of information and communication technologies to industry and is also known as "Industry 4.0". It builds on the developments of the Third Industrial Revolution.


## Industry 3.0 and Industry 4.0
1. **Industry 3.0**  
<img src="https://themanufacturer-cdn-1.s3.eu-west-2.amazonaws.com/wp-content/uploads/2019/12/14113222/Third.jpg" width="800" height="500">

* The Industries in this era used modern techniques like PLC, SCADA, etc. for the ease in manufacturing units.
* The Automation was introduced in the industries but Human presence was required to look after this automation output and control them.
* They used electronic components and IT systems.

2. **Industry 4.0**  
<img src="https://www.ame.org/files/industry_4.0_0.jpg" height="500" width="600">  

* Industry 4.0 is the industry 3.0 connected to internet..
* The Fourth industrial Revolution is the era of smart machines, storage systems and production facilities that can autonomously exchange information,
trigger actions and control each other without human intervention.
* This revolution interacts with the cloude services and provides data. Also, it accepts commands from the cloud end.
* This data sharing and interaction of machines helps to increase the efficiency using data analysis.
* The Industries of this era is called Smart Industry.

## Architecture of 3.0 and 4.0
![both](images/assignments_summary_pictures_pyramid.png)


### Inside of Architecture of Industry 4.0
![Ind4.0](images/PhotosinUse_industrial4.0.png)
## How machines communicate?
 The various machines communicates using various communication protocols.
1. **Communication Protocol In industry 3.0**
 * Modbus
 * Profinet
 * EtherCAT
 * Canopen
<img src="https://i0.wp.com/ecnautomation.com/wp-content/uploads/2019/09/PROTOCOLOS-1.png?fit=752%2C333&ssl=1" width="900" height="500">

2. **Communication Protocol In industry 4.0**
 * Mqtt
 * Http
 * CoAP

![protocols-in-industry4.0](images/assignments_summary_pictures_proto4.png)
## Conversion
We will convert industry 3.0 protocols to industry 4.0 protocols.

There are few challanges in this conversion.
* Expensive Hardware
* Lack of Documentation
* Properitary of PLC protocols
## Solution
We have a library that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud.  
We can use this library in IoT devices and so the problems will be solved.  
Hardware cost will be lowered.  
Conversion of communication protocol.  

![solution](images/assignments_summary_pictures_bs.png)

## How this happens
Step 1: Identify Most popular indutry 3.0 devices.  
Step 2: Study protocols that these device communicate with.  
Step 3: Get data from industry 3.0 devices.  
Step 4: Send data to cloud for industry 4.0.  

### What all things can be done after Collection of data:
<img src="https://www.spectralengines.com/hs-fs/hubfs/Imported_Blog_Media/SE_artikkeli_kuva_SmartIndustry_v01b-1024x538.png?width=1024&height=538&name=SE_artikkeli_kuva_SmartIndustry_v01b-1024x538.png">
![Image-of-adv-4.0](images/adv4.png)

### Analysis tools for Collected data:

**IOT TSDB (Time Series Database)**: To store data
* Prometheus
* InfluxDB  

**IOT Dashboard**: To view your data in a better way
* Grafana
* Thingsboard

**IOT Cloud Platforms**:
* AWS IoT
* Google IoT
* Azure Iot
* Thingsboard

**Get Alerts**: Get alerts based on your data
* Zaiper
* Twilio
